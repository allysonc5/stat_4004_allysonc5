---
title: "MidTerm_allyson5"
author: "Allyson Cloninger"
date: "March 20, 2019"
output: pdf_document
---

```{r setup, include = FALSE}
library(plyr)
library(tidyr)
library(data.table)
library(lubridate)
library(rvest)
library(readxl)
library(knitr)
library(quantmod)
library(rtweet)
library(syuzhet)
library(gtools)
library(stringr)
library(ggplot2)
library(gridExtra)
library(twitteR)
```

## Problem 1
## part A
```{r prob1a}
url1 <- "C:/Users/Allyson/Documents/school/spring 2019/STAT 4004/BOING-BOING-CANDY-HIERARCHY-2016-SURVEY-Responses.xlsx"
data2016 <- read_excel(url1)
## get rid of unnecessary columns
data2016 <- data2016[,c(-(1:6),-(107:123))]
## add in year variable
data2016$year <- 2016

url2 <- "C:/Users/Allyson/Documents/school/spring 2019/STAT 4004/CANDY-HIERARCHY-2015-SURVEY-Responses.xlsx"
data2015 <- read_excel(url2)
## getting rid of unnecessary columns
data2015 <- data2015[,c(-(1:3),-(97:113),-(116:124))]
data2015$year <- 2015
##checked which candies different in 2016 vs. 2015
##setdiff(names(data2016), names(data2015))

## renamed some of columns in 2015 dataset to match names used in 2016
## bonkers (data2016 = 5, data2015 = 6)
colnames(data2015)[6] <- colnames(data2016)[5]
## box o raisins (data2016 = 8, data2015 = 8)
colnames(data2015)[8] <- colnames(data2016)[8]
## hershey's dark chocolate (data2016 = 34, data2015 = 19)
colnames(data2015)[19] <- colnames(data2016)[34]
## black licorice (data2016 = 49, data2015 = 44)
colnames(data2015)[44] <- colnames(data2016)[49]
## joyjoy mit iodine (data2016 = 40, data2015 = 54)
colnames(data2015)[54] <- colnames(data2016)[40]
## sweetums (data2016 = 88, data2015 = 82)
colnames(data2015)[82] <- colnames(data2016)[88]

url3 <- "C:/Users/Allyson/Documents/school/spring 2019/STAT 4004/candyhierarchy2017.xlsx"
data2017 <- read_excel(url3)
data2017 <- data2017[,c(-(1:6),-(110:120))]
data2017$year <- 2017
## additional cleaning of column names
## get rid of Q6| that came before candy name
colnames(data2017)[1:103]<- sapply(strsplit(colnames(data2017)[1:103],"\\|"), '[', 2)
# get rid of leading white space before start of candy name
colnames(data2017)[1:103] <- trimws(colnames(data2017)[1:103], which = "left")
## add brackets before and after candy name to match the formatting of 2015/2016 column names
colnames(data2017)[1:103] <- paste("[", colnames(data2017)[1:103], "]", sep = "")

## check which candies are different in 2017 vs. other years
##setdiff(names(data2017), names(data2016))
##setdiff(names(data2017), names(data2015))

## rename anonymous globs aka Mary Janes to name that was used in 2016/2015
colnames(data2017)[2] <- colnames(data2016)[2]

## convert all datasets to dataframes to make merging easier
data2016 <- data.frame(data2016)
data2015 <- data.frame(data2015)
data2017 <- data.frame(data2017)

## combine all datasets
alldata <- smartbind(data2016, data2015, data2017)

## column names have X before them and periods separating the words
## will clean this up by removing X and replacing periods with spaces
colnames(alldata)[1:5]
## keep all parts of column name after X
colnames(alldata) <- substring(colnames(alldata), 3)
## replace periods with spaces for all column names
colnames(alldata) <- gsub("\\.", " ", colnames(alldata))
## fix year column name since got messed up with substring part
colnames(alldata)[101] <- "year"

## do similar cleaning as above for column names on each dataset
## since going to use these datasets later for ratings by year
colnames(data2016) <- substring(colnames(data2016), 3)
colnames(data2016) <- gsub("\\.", " ", colnames(data2016))
colnames(data2016)[101] <- "year"

colnames(data2015) <- substring(colnames(data2015), 3)
colnames(data2015) <- gsub("\\.", " ", colnames(data2015))
colnames(data2015)[96] <- "year"

colnames(data2017) <- substring(colnames(data2017), 3)
colnames(data2017) <- gsub("\\.", " ", colnames(data2017))
colnames(data2017)[104] <- "year"
```

## part B
##1.
Most of the issues I faced involved the cleaning aspect of the data. The different column naming across the years was tedious to clean up and make sure that there weren't duplicate columns for candies just because of a different naming conventoin. My approach to correct this was to see if the column differences between datasets were additions of new candies, or just changing the phrasing of a candy that was already asked about. For instance, using setdiff, I could see that both the 2015 and 2017 datasets had the "anonymous brown globs" candy, though the 2017 dataset added "aka Mary Janes" to the column name. Little differences like this were insignificant, so I changed the column names to match across the variables if that makes sense. 

Once I got all the column names how I wanted them, merging the datasets was relatively easy. From doing research on the internet, I found that the function smartbind() was a good way to merge datasets that had different columns and rows without losing any of the observations. This method adds NA to any of the observations where it does not have a value for that column. After I got all the datasets combined this way, I cleaned up the column names even further by removing the X that was added before each column name when I converted the datasets to dataframes. I also had to convert periods that separated the words into spaces. 

This wasn't a perfect approach, as I lost some of the original punctuation in the names (like apostrophes). This was fine by me though since all the names of the candies are still readable. All in all, the data cleaning was the most tedious part of this question.

##2.
My cleaned dataset contains 119 candy questions. I removed any columns that were not asking about the specific candy, like the ones containing country, gender, whether they went trick or treating, and all of the unnecessary questions at the end of the datasets. I used this approach since we really only care about the joy, despair, and meh counts across the candies.

##3.
```{r prob1b}
counts <- rep(NA,length(alldata))
## count number of non-NA observations for each column
for (i in 1:length(counts)){
  counts[i] <- length(na.omit(alldata[,i]))
}
## make dataframe of these counts for each type of candy
dfcounts <- data.frame(colnames(alldata))
colnames(dfcounts) <- "type"
dfcounts$counts <- counts
## top 10 candies by number of reviews
## had to do 2-11 since the first observation was the year since every observation had a year
dfcounts[order(dfcounts$counts, decreasing = T)[2:11],]
```

As seen above, these are the top 10 candies when it comes to number of reviews. From 1-10, the most reviewed candies were Reese's peanut butter cups, any full-sized candy bar, Kit Kat, cash, snickers, peanut M&Ms, anonymous brown globs, dental paraphenalia, Hershey's Dark Chocolate, and Cadbury Creme eggs. The most amount of reviews overall was 8386 for Reese's Peanut Butter cups.

##4.
```{r prob1d}
dfcounts[order(dfcounts$counts)[1:10],]
```

Now looking at the top 10 least reviewed candies by number of reviews, they are Person of Interest Season 3, third party M&Ms, abstained from M&Ming, Take 5, independent M&Ms, sandwhich bags with BooBerry Crunch, green party M&Ms, Real Housewives of Orange County Season 9, Bonkers the board game, and Coffee Crisp. The least amount of reviews overall was 1136 for Person of Interest season 3.

##5.
To figure out the ratings as well as other stats on the candies, I made a function called rateCandy. In this function, I create a dataframe that has the following stats for each candy: overall count of reviews, the rating (sum of all joy(+1) and despair(-1) ratings), a count of all the joy responses, normalized ratings which will be used for each year where normalized=ratings/count of reviews, and a fraction of joy variable that is the count of joy responses/overall count of reviews.
```{r prob1ratings}
rateCandy <- function(data){
  ## vector that will hold ratings for each candy
  ratings <- rep(0, length(data))
  ## vector that will hold overall counts of total reviews for each candy
  counts <- rep(0, length(data))
  ## vector that will hold joy counts for each candy
  joy <- rep(0, length(data))
  ## for loop that goes by each row of each column
  for (i in 1:length(data)){
    ## update count of total reviews for each candy
    counts[i] <- length(na.omit(data[,i]))
    for (j in 1:nrow(data)){
      ## if the observations is not NA, will figure out if joy or despair to update ratings
      if (!is.na(data[j,i])){
        ## if joy, add 1 to sum of ratings and increase joy counter
        if (data[j,i] == "JOY"){
          ratings[i] <- ratings[i] + 1
          joy[i] <- joy[i] +1
        }
        ## if despair, subtract 1 from sum of ratings
        if (data[j,i] == "DESPAIR"){
          ratings[i] <- ratings[i] - 1
        }
      }
    }
  }
  ## make data frame where first column is the names of all the candies
  dfratings <- data.frame(colnames(data))
  ## rename names of candy column to type
  colnames(dfratings) <- "type"
  ## add the ratings for each candy
  dfratings$ratings <- ratings
  ## add the counts for each candy
  dfratings$counts <- counts
  ## normalize the ratings by total counts for each candy
  dfratings$normalized <- ratings/counts
  ## add the joy counts for each candy
  dfratings$joy <- joy
  ## calculate proportion of joy for each candy
  dfratings$fracjoy <- joy/counts
  ## return the dataframe with all these stats
  return(dfratings)
}
## rate candy overall across all the years
rate <- rateCandy(alldata)
## top 10 best overall
bestall <- rate[order(rate$ratings, decreasing = T)[1:10],]
bestall

## top 10 worst overall
worstall <- rate[order(rate$ratings)[1:10],]
worstall
```

Across all the years, the top 10 best rated candies were any full sized candy bar, Reese's Peanut butter cups, Kit Kat, Twix, Snickers, Cash, Peanut M&Ms, Regular M&Ms, Nestle Crunch, and Tolberone. The full-sized candy bar had the highest rating of 7182 across all the observations.

Across all the years, the top 10 worst rated candies were broken glow stick, gum from baseball cards, candy that was given out for free at restaurants, kale smoothie, white bread, dental paraphenalia, creepy religious comics, whole what anything, anonymous brown globs, and box of raisins. The broken glow stick has the worst rating of -7757 across all the observations.

##6.
I then used my function on the 2015, 2016, and 2017 datasets.
```{r prob1ratingsyear}
rate2015 <- rateCandy(data2015)
best2015 <- rate2015[order(rate2015$ratings, decreasing = T)[1:10],]
best2015
worst2015 <- rate2015[order(rate2015$ratings)[1:10],]
worst2015
```

The top 10 best rated candies for 2015 were any full sized candy bar, Reeses, Kit Kat, Twix, Snickers, Cash, Peanut M&Ms, Regular M&Ms, Nestle Crunch, and Butterfinger.

The top 10 worst rated candies for 2015 were broken glow stick, candy that is given out free at restaurants, gum from baseball cards, kale smoothie, dental paraphenalia, white bread, Creepy religious comics, anonymous brown globs, spotted dick, and lapel pins.

```{r ratings2016}
rate2016 <- rateCandy(data2016)
best2016 <- rate2016[order(rate2016$ratings, decreasing = T)[1:10],]
best2016
worst2016 <- rate2016[order(rate2016$ratings)[1:10],]
worst2016
```

The top 10 best rated candies in 2016 were any full sized candy bar, cash, Kit Kat, Reeses, Twix, Snickers, Lindt Truffle, Tolberone, Peanut M&Ms, and Milky Way.

The top 10 worst rated candies in 2016 were broken glow stick, white bread, gum from baseball cards, kale smoothie, candy that is given out free at restaurants, dental paraphenalia, whole wheat anything, anonymous brown globs, odd marshmallow circus peanuts, and box of raisins.

```{r ratings2017}
rate2017 <- rateCandy(data2017)
best2017 <- rate2017[order(rate2017$ratings, decreasing = T)[1:10],]
best2017
worst2017 <- rate2017[order(rate2017$ratings)[1:10],]
worst2017
```

The top 10 best rated candies of 2017 were any full sized candy bar, Reeses, Kit Kat, cash, Twix, Snickers, Tolberone, Lindt Truffle, Peanut M&Ms, and Nestle Crunch.

The top 10 worst rated candies of 2017 were broken glow stick, gum from baseball cards, white bread, Real Housewives of Orange County Season 9, dental paraphenalia, kale smoothie, candy that is given out for free at restaurants, whole wheat anything, creepy religious comics, and odd marshmallow circus peanuts.

##7.
I used ggplot to create barplots of the top 10 best/worst candies overall and separately by year.
## Overall Barplots
```{r plots}
ggplot(bestall, aes(x=type, y=ratings, fill = type)) + geom_bar(stat="identity") + theme(axis.text.x = element_blank()) + ggtitle("Top 10 Overall Best Rated Candies")

ggplot(worstall, aes(x=type, y=ratings, fill = type)) + geom_bar(stat="identity") + theme(axis.text.x = element_blank()) + ggtitle("Top 10 Overall Worst Rated Candies")
```

##8.
## Normalized Best/Worst Rated Candies by Year
## 2015 Barplots
```{r plots2}
ggplot(best2015, aes(x=type, y=normalized, fill = type)) + geom_bar(stat="identity") + theme(axis.text.x = element_blank()) + ggtitle("2015's Top 10 Normalized Best Rated Candies")

ggplot(worst2015, aes(x=type, y=normalized, fill = type)) + geom_bar(stat = "identity") + theme(axis.text.x = element_blank()) + ggtitle("2015's Top 10 Normalized Worst Rated Candies")
```

##2016 Barplots
```{r plots3}
ggplot(best2016, aes(x=type, y=normalized, fill = type)) + geom_bar(stat="identity") + theme(axis.text.x = element_blank()) + ggtitle("2016's Top 10 Normalized Best Rated Candies")

ggplot(worst2016, aes(x=type, y=normalized, fill = type)) + geom_bar(stat = "identity") + theme(axis.text.x = element_blank()) + ggtitle("2016's Top 10 Normalized Worst Rated Candies")
```

## 2017 Barplots
```{r plots4}
ggplot(best2017, aes(x=type, y=normalized, fill = type)) + geom_bar(stat="identity") + theme(axis.text.x = element_blank()) + ggtitle("2017's Top 10 Normalized Best Rated Candies")

ggplot(worst2017, aes(x=type, y=normalized, fill = type)) + geom_bar(stat = "identity") + theme(axis.text.x = element_blank()) + ggtitle("2017's Top 10 Normalized Worst Rated Candies")
```

##9.
As seen throughout my analysis, some of the most popular candies appear to be full sized candy bars, cash, Reeses, and Kit Kats. Some of the least popular candies are broken glow sticks, candy given out for free at restaurants, kale smoothie, and gum from baseball cards.

## part C
If we care about the change in joy preferences from 2016 to 2017, we can combine the 2016/2017 dataset stats and calculate the change in the fraction of joy.
```{r prob1c}
## merge the 2016 and 2017 datasets by type
## only keeps the observations they have in common which is fine since we need to calculate change in joy proportion
joychange <- merge(rate2016, rate2017, by = "type")
## calculate difference between joy proportion of 2017 and joy proportion of 2016
joychange$diff <- joychange$fracjoy.y - joychange$fracjoy.x
## sort in decreasing order by difference and print out the largest gain in joy prefrence
joychange[order(joychange$diff, decreasing = T)[1],]
## sort in ascending order by difference and print out worst gain in joy preference
joychange[order(joychange$diff)[1],]
```

From 2016 to 2017, Nestle Crunch saw the most gain in preference with a joy proportion increase of 5.68%, while Glow sticks saw the worst gain in preference with a joy proprtion decrease of 6%.

## Problem 2
Below is the stock price data for Hershey from June 2016 to the end of June 2018.
```{r prob2}
getSymbols("HSY", src="yahoo")
chartSeries(HSY, subset = '2016-06::2018-06')
```

It looks like there are a few peaks, like around July 2016, June 2017, and December 2017.

## Problem 3
For doing the sentiment analysis on tweets, I decided to exclude retweets because of them not being original tweets and hence becoming repetitive if there are a lot of them.
```{r prob3}
## setup up twitter authentication
setup_twitter_oauth(consumer_key="F5jWYx9DULqweOPPeTB7148Ud",
        consumer_secret="mIw2cqAYkt8CecGtLeT0HMApqwnUsbsbRRG7Z68tgVPkUN7vVZ",
        access_token="2255903984-z0qZUUdXobq9ambcwkIUhq4BfaSIxagMqVsFIJK", 
        access_secret="9JWsPeBZRha2XsrI8puXugdysjxqIrozAx69GoMOSw4z2")

## pull tweets from previous 5 days that have djia in them
## i at first set n to 10000 because I wasn't sure how many tweets there were in previous 5 days
## then got error message saying only 736 tweets with djia so that's what I said n equal to
tweets <- searchTwitter(searchString="djia -filter:retweets", n=736, since = '2019-03-15', until = '2019-03-20', lang='en')

# Dumping twitter data into a data frame
tweets.df = twListToDF(tweets)
##View(tweets.df)

## getting the plain text only
tweets.df$plain_text <- plain_tweets(tweets.df$text)

## get sentiment for each tweet
sentiment <- get_nrc_sentiment(tweets.df$plain_text)
tweets_all <- as.data.frame(cbind(tweets.df, sentiment))

## keep date only (first 10 characters) instead of date and time so I can do boxplot by date
tweets_all$created <- as.character(tweets_all$created)
tweets_all$created <- substring(tweets_all$created, 1, 10)

## calculate the net sentiment for each tweet
tweets_all$sentimentnet <- tweets_all$positive - tweets_all$negative

## plot DJIA price during 5 day span
getSymbols("DJIA", src="FRED")
chartSeries(DJIA, subset = '2019-03-15::2019-03-19')

## boxplot of sentiment scores
boxplot(sentimentnet~created, data = tweets_all, main = "Boxplot of Sentiment Scores by Day")
```

The DJIA price appears to hit a high on the 18th of March which also coincides with a larger range of positive sentiment scores for the 18th as seen in the above box plot. The highest sentiment score of 5 occurs on the day that the price peaks. On the 19th when the price starts to drop again, it appears that tweets are generally more negative which is what you'd expect. All in all, it appears that doing a sentiment analysis of tweets can give a general idea on how the Dow Jones price is doing.
